import collections from './model-collections.js'
import articles from './model-articles.js'
import projects from './model-projects.js'
import pages from './model-pages.js'
import configs from './model-configs.js'

export default [
 articles,
 collections,
 projects,
 pages,
 configs
]
