export default {
    label: 'Price',
    name: 'price',
    widget: 'number',
    required: false,
    hint: 'The price for this article.'
}
