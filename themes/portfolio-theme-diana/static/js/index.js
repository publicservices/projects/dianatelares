import ImagesLoaded from './utils/images-loaded.js'
import JavascriptCheck from './utils/javascript-check.js'
import NetlifyIdentity from './components/netlify-identity.js'

const netlifyIdentity = document.createElement('netlify-identity')

const $body = document.querySelector('body')
$body.appendChild(document.createElement('javascript-check'))
$body.appendChild(document.createElement('images-loaded'))
$body.appendChild(document.createElement('netlify-identity'))

export default {}
