+++
layout = "no-title"
title = "Orders"

+++
Order information:

To the items here the current time for shipping is one week. If you are interested in a custom order the details will be specificated in the email.



* Free shipping in Mallorca, we will bring the pieces to you!
* 10 € shipping in Europe for orders below 100€
* Free shipping in Europe for orders over 100€
* Free shipping worldwide for order over 200€
* We are delighted to answer any question, send a message at dianatelares@gmail.com
