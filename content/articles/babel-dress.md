+++
title = "Babel on black"
slug = "babel-on-black"
draft = true
date_release = "2020-04-04"
price = 60
paypal = "5X44GVA2WRLDU"
has_stock = true
sizes = ["s", "l"]
collection = "collections"
image = "/media/uploads/dscf5418.jpg"

[[gallery]]
image = "/media/uploads/dscf5419-2.jpg"

[[gallery]]
image = "/media/uploads/dscf5418.jpg"

[[gallery]]
image = "/media/uploads/dscf5414.jpg"

[[gallery]]
image = "/media/uploads/dscf5423.jpg"

[[gallery]]
image = "/media/uploads/dscf5243.jpg"
+++
The Babel dress is a confy and fresh dress confectioned with a Coban gauze black cotton fabric embroidered in diferents colors. Sitting just below the knee, open in the front with red velvet buttons, with side interior pockets and a scooped back neck. Handmade in Guatemala, through [Pixan](https://www.pixan.design/) association.

Unique model produced

We are delighted to answer any questions, help with the size guide and take your orders at dianatelares@gmail.com adress.
