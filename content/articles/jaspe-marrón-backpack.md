+++
title = "Jaspe brown"
slug = "jaspe-brown"
draft = true
date_release = "2020-04-08"
price = 70
paypal = "6ER3M3HYQTRHN"
has_stock = true
collection = "GentryonGuatemala"
image = "/media/uploads/7.jpg"

[[gallery]]
image = "/media/uploads/6.jpg"

[[gallery]]
image = "/media/uploads/7.jpg"

[[gallery]]
image = "/media/uploads/dscf5197b.jpg"
+++
Daily square backpack, elaborated with cotton, original from Salcajá, Guatemala. The fabric is mostly brown, black and white colors, hand weaved in a pedal loom; the original fabric is used in the corte (skirt) that the woman use in the region. It has four pockets, 3 auxiliary (one on the top, one the front and another on the side) and a main one. IKK metal zippers with adjustable handles. Handmade in Guatemala, throught [Pixan](https://www.pixan.design/) association.

\ Size: 30 cm width, 30 cm heigh, 11 cm depth\
Volume : 20 L

Unique Model produced
