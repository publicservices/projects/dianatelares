+++
title = "Margarita White and Blue"
slug = "margarita-white-and-blue"
date_release = "2020-04-12"
price = 90
paypal = "3LZL7HV964JU4"
sold_out = true
sizes = ["m", "l"]
collection = "collections"
image = "/media/uploads/dscf2642.jpg"

[[gallery]]
image = "/media/uploads/dscf2639.jpg"

[[gallery]]
image = "/media/uploads/dscf2654.jpg"

[[gallery]]
image = "/media/uploads/dscf2642.jpg"

[[gallery]]
image = "/media/uploads/dscf5353.jpg"

[[gallery]]
image = "/media/uploads/dscf5382.jpg"

[[gallery]]
image = "/media/uploads/dscf5389.jpg"

[[gallery]]
image = "/media/uploads/dscf5085.jpg"

[[gallery]]
image = "/media/uploads/dscf5091.jpg"
+++
The Margarita dress is a midi white dress confectioned with the Coban cotton gauze fabric, combined in the top back with a hand embroidered blue tones canvas, originally used in the Huipils of Quetzaltenango. It is open in the front with three concha nacar buttons, side interior pockets and a scooped back neck. Handmade in Guatemala and sewed by Margarita Guzmán, who gives de name to the piece, throught [Pixan](https://www.pixan.design/) association.

There are three pieces produced in this model.

We are delighted to answer any questions, help with the size guide and take your orders at dianatelares@gmail.com adress.
