+++
title = "Babel white"
slug = "babel-white"
draft = true
date_release = "2020-04-04"
price = 60
paypal = "XP3GW996K8Y26"
has_stock = true
collection = "collections"
image = "/media/uploads/dscf5304.jpg"

[[gallery]]
image = "/media/uploads/dscf5306.jpg"

[[gallery]]
image = "/media/uploads/dscf5309.jpg"

[[gallery]]
image = "/media/uploads/dscf5311.jpg"

[[gallery]]
image = "/media/uploads/dscf5314.jpg"

[[gallery]]
image = "/media/uploads/dscf5283.jpg"

[[gallery]]
image = "/media/uploads/dscf5286.jpg"

[[gallery]]
image = "/media/uploads/dscf5295.jpg"
+++
The Babel dress is a confy and fresh dress confectioned with a Coban gauze white cotton fabric embroidered in soft yellow, combined in the top with a white & yellow hand weave Ikat cotton fabric. Sitting just below the knee, open in the front with wood buttons, with side interior pockets and a scooped back neck. Handmade in Guatemala, through [Pixan](https://www.pixan.design/) association.

Unique model produced

We are delighted to answer any questions, help with the size guide and take your orders at dianatelares@gmail.com adress.
