+++
title = "Jaspe Blue"
slug = "jaspe-blue"
draft = true
date_release = "2020-04-08"
price = 70
paypal = "JNLBT4U875QCQ"
has_stock = true
collection = "GentryonGuatemala"
image = "/media/uploads/9.jpg"

[[gallery]]
image = "/media/uploads/8.jpg"

[[gallery]]
image = "/media/uploads/9.jpg"

[[gallery]]
image = "/media/uploads/c5.jpg"

[[gallery]]
image = "/media/uploads/133-2-.jpg"

[[gallery]]
image = "/media/uploads/captura-de-pantalla-2020-01-28-a-las-9.49.02.png"

[[gallery]]
image = "/media/uploads/dscf2038-1-.jpg"
+++
Daily square backpack, elaborated with a cotton textile, original from Salcajá, Guatemala. The fabric combine blue and grey tone colors, hand weaved in a pedal loom; the original fabric is used in the corte (skirt) that the woman use in the region. It has four pockets, 3 auxiliary (one on the top, one the front and another on the side) and a main one. IKK metal zippers with adjustable handles. Handmade in Guatemala, throught [Pixan ](https://www.pixan.design/)association.

/    Size: 30 cm width, 30 cm heigh, 11 cm depth\
    Volume : 20 L

Unique Model produced
