+++
title = "while isolation"
slug = "whileisolation"
draft = false
date_release = "2021-08-01"
image = "/media/uploads/p1160019.jpg"
+++
<iframe width="713" height="401" src="https://www.youtube.com/embed/tj8dpIlXxDc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

La tela... se posa,\
las prendas que cubren la desnudez, que protegen y esconden las verdades,\
que rozan la piel, que acompañan al clima, y adorna toda una vida.\
Cuando no es ligera, sino caprichosa, procura no sucumbir sin seño a su encanto, pues esconde, y es lo mas probable, intenciones ... 



During the isolation of March and April 2019, I was sewing a series of garments for my wardrobe, recovering fabrics from many years ago arriving from various parts of the world.
